//////////////////////////////////////////////////////////////////////////////
//Crea la representaci�n MPM correspondiente a un submodelo MMS ARIMA 
// * output y transformaci�n
// * parte ARIMA y varianza con prior inverse-chi
// * fitro lineal aditivo
// * fitro lineal principal
// * priors normales escalares y restricciones de intervalo sobre
//  * par�metros indicviduales 
//  * combinaciones lineales de par�metros que le afectan de forma exclusiva.
// 
MPM::@Model BuildTFARIMASubModel(
  MPM::@Config cfg,        //Configuraci�n de MPM
  MMS::@Submodel subModel, //Submodelo MMS
  Set combInfo)            //Combinaciones MMS exclusivas del submodelo  
//////////////////////////////////////////////////////////////////////////////
{
  Text subModId = subModel::GetIdentifier(?);
  WriteLn("[Mms2Mpm::BuildTFARIMASubModel] Building submodel "<<subModId+" ...");
  //Time info
  NameBlock interval = subModel::GetInterval(?);
  @TimeSet dating = [[interval::GetDomain(?)]];
  Date begin = interval::GetBegin(?);
  Date end = interval::GetEnd(?);
  //Output info
  @NameBlock out = [[ subModel::GetOutput(?) ]];
  @Serie output.orig = $out::GetVariable(?)::GetData.(100);
  VMatrix Y.orig   = Mat2VMat(SerSetMat(output.orig,  begin,end),True);
  NameBlock transf = If(FunctionExist("NameBlock","subModel::GetTransformation"),
  {
    subModel::GetTransformation(?)
  },
  {
    $out::GetTransformation(?)
  });
  VMatrix Y.transf = transf::Direct(Y.orig);
  Set transfSpec = transf::GetSpecification(?);
  Real boxcox_add = transfSpec::Arguments::_.second;
  Real boxcox_exp = transfSpec::Arguments::_.first;

  //Noise info
  NameBlock noise = subModel::GetNoise(?);
  Set arimaFactors = noise::GetInitialARIMA(?);
  Polyn dif = ARIMAGetDIF(arimaFactors);

  //active explicative terms
  Set ets = subModel::GetExpTerms_Active(?);
  //splitting explicative terms by by subclass, additive/non additive
  Real isLin   (MMS::@ExpTerm et) { et::GetSubclass(?)=="Linear" };
  Real isRat   (MMS::@ExpTerm et) { et::GetSubclass(?)=="Ratio" };
  Real isAdd   (MMS::@ExpTerm et) { et::IsAdditive(?) };
  Real isNonAdd(MMS::@ExpTerm et) { Not(et::IsAdditive(?)) };
  Set lin = Select(ets, isLin);
  Set rat = Select(ets, isRat);
  Set linNonAdd = Select(lin, isNonAdd);
  Set ratNonAdd = Select(rat, isNonAdd);
  Set linAdd = Select(lin, isAdd);
  Set ratAdd = Select(rat, isAdd);
  Real hasAdd = Or(Card(linAdd),Card(ratAdd));

  //Splitting combinations by subclass, additive/non additive
  Set ci.extern = Select(combInfo,Real(Set ci) { ci::subClass=="" });
  Set ci.lin = Select(combInfo,Real(Set ci) { ci::subClass=="Linear" });
  Set ci.linNonAdd = Select(ci.lin,Real(Set ci) { Not(ci::isAdditive) });
  Set ci.linAdd = Select(ci.lin,Real(Set ci) { ci::isAdditive });

  //Building model 
  @NameBlock mpm = MPM::@Model::NewRef(subModId,cfg);
  Real If(And(hasAdd,boxcox_exp!=0),
  {
    Error("Additive explicative terms are allowed only for "
          "logarithmic transformations");
    False
  },
  {
    //Main filter related modules
    @NameBlock MainFilter = If(hasAdd,
      Mms2Mpm::BuildFilteredModule      (subModId+".MainFilter", Y.transf),
      Mms2Mpm::BuildTransfFilteredModule(subModId+".MainFilter", Y.orig, transfSpec));
    @NameBlock MainLinear = 
      Mms2Mpm::BuildLinearModule(subModId+".MainLinear", linNonAdd, interval);
    Real AddCombSet($MainLinear,ci.linNonAdd);
    Set MainRatios =
      Mms2Mpm::BuildRatioModules(ratNonAdd, interval);
    //Noise related modules
    @NameBlock Arima = Mms2Mpm::BuildArima(subModId+".MainArima",noise,Y.transf);
    @NameBlock Sigma2 = Mms2Mpm::BuildArimaSigma2(subModId+".MainSigma2",noise,$Arima::getDifNoise(?));
    Real $mpm::addSigmaArima($Sigma2,$Arima);
    Real If(hasAdd,
    {
      //Original filter related modules
      @NameBlock OrigFilter = Mms2Mpm::BuildLogAdditiveFilteredOutputModule(
        subModId+".OrigFilter", Y.orig, boxcox_add);
      @NameBlock OrigLinear = Mms2Mpm::BuildLinearModule(
        subModId+".OrigLinear", linAdd, interval);
      Real AddCombSet($OrigLinear,ci.linAdd);
      Set OrigRatios =
        Mms2Mpm::BuildRatioModules(ratAdd, interval);
      Real $mpm::addFilterModules($OrigFilter, OrigLinear<<OrigRatios);
      @NameBlock rel = MPM::@Relation::NewRef_2(
        "@RelationSetTransfFiltered",
        $OrigFilter::_.name+"."+$MainFilter::_.name,
        $OrigFilter,
        $MainFilter);
      Real { $mpm::addRelation($rel)};
      True
    });
    Real Mms2Mpm::AddMissingModule($mpm, subModId+".MissingOutput", 
      MainFilter, $MainFilter::_.output,
      $out::GetParametersMissing(?), interval);
    Real $mpm::addFilterModules($MainFilter, MainLinear<<MainRatios);
    Real $mpm::addFilter2ArimaRelation($MainFilter,$Arima);
    Real BuildExternCombSetModule($mpm,ci.extern);

    Real $mpm::setup(?);
    Real $mpm::set.x($mpm::build.x(?));
    True
  });
  $mpm
};

//////////////////////////////////////////////////////////////////////////////
//Crea la representaci�n MPM correspondiente a un modelo MMS ARIMA con uno o
//varios submodelos y en tal caso con posibles estructuras jer�rquicas. 
@NameBlock BuildTfArimaFullModel(MPM::@Config cfg, MMS::@Model mms)
//////////////////////////////////////////////////////////////////////////////
{
  Text name = mms::GetIdentifier(?); 
  WriteLn("[Mms2Mpm::BuildTfArimaFullModel] Building "<<
    Card(mms::GetSubmodels(?))+" sub-models of model "<<name+
    " with these settings:\n" << cfg);
  Set comInfo = Mms2Mpm::GetActiveCombinationsInfo(mms);
  Set subModelsMMS = mms::GetSubmodels(?);
  Set subModels = For(1,Card(subModelsMMS),MPM::@Model(Real s)
  {
    Set combInfoSel = Select(comInfo,Real(Set ci)
    {
      ci::subMod.idx==s
    });
    BuildTFARIMASubModel(cfg, subModelsMMS[s], combInfoSel)
  });
  @NameBlock mpm = If(Card(subModels)==1,
  {
    //Si hay un solo submodelo no se necesita crear un modelo de nivel 
    //superior que lo incluya sino que se le pueden aplicar directamente
    //las jerarqu�as si las hubiere
    @NameBlock mpm = subModels;
    Real AddLinearHierarchies($mpm, mms);
    mpm
  },
  { 
    //Si hay m�s de un modelo hay que crear un modelo jer�rquico que los
    //incluya como m�dulos, as� como las jearqu�as y combinaciones lineales
    //que afecten a m�s de un submodelo
    WriteLn("[Mms2Mpm::BuildTfArimaFullModel] Building hierarchies ...");
    @NameBlock mpm = MPM::@HierarchicalLinear::NewRef(name,cfg);
    NameBlock PutName(name,$mpm);
    Set EvalSet(subModels, Real(MPM::@Model sm) { $mpm::addSubModel(sm) });
    Real AddLinearHierarchies($mpm, mms);
    Set externCombInfo = Select(comInfo,Real(Set ci) { ci::subMod.idx==0 });
    Real BuildExternCombSetModule($mpm,externCombInfo);
    mpm
  });
  //Se inicializa el objeto MPM para que est� listo para ser usado
  Real $mpm::setup(?);
  mpm
};
